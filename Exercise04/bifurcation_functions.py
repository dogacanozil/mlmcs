
def andronov_hopf_bifurcation(t, x, alpha):
    """
    Calculates the Andronov-Hopf bifurcation from given x and returns the velocities x1_dot and x2_dot.
    Parameters
    ----------
    t: numpy.array
            Time values of the system
    x: Tuple of numpy.array
            x1 and x2 values
    alpha: numpy.linspace
            Parameter of the system
    """
    x1, x2 = x
    x1_dot = alpha*x1 - x2 - x1*(x1**2 + x2**2)
    x2_dot = x1 + alpha*x2 - x2*(x1**2 + x2**2)
    return [x1_dot, x2_dot]
