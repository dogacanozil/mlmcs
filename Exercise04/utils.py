import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


def solve_euler(f_ode, y0, time):
    """
    Solves the given ODE system in f_ode using forward Euler.
    :param f_ode: the right hand side of the ordinary differential equation d/dt x = f_ode(x(t)).
    :param y0: the initial condition to start the solution at.
    :param time: np.array of time values (equally spaced), where the solution must be obtained.
    :returns: (solution[time,values], time) tuple.
    """
    yt = np.zeros((len(time), len(y0)))
    yt[0, :] = y0
    step_size = time[1]-time[0]
    for k in range(1, len(time)):
        yt[k, :] = yt[k-1, :] + step_size * f_ode(yt[k-1, :])
    return yt, time


def plot_phase_portrait(A, X, Y):
    """
    Plots a linear vector field in a streamplot, defined with X and Y coordinates and the matrix A.
    """
    UV = A@np.row_stack([X.ravel(), Y.ravel()])
    U = UV[0,:].reshape(X.shape)
    V = UV[1,:].reshape(X.shape)

    fig = plt.figure(figsize=(15, 15))

    # gs = gridspec.GridSpec(nrows=3, ncols=2, height_ratios=[1, 1, 2])

    #  Varying density along a streamline
    # ax0 = fig.add_subplot(gs[0, 0])
    ax0 = fig.add_subplot()
    ax0.streamplot(X, Y, U, V, density=[0.5, 1])
    ax0.set_title('Streamplot for linear vector field A*x');
    ax0.set_aspect(1)
    return ax0

def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
            For portrait A4, width=595
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)

def multicolored_line(x, y, z, ax, s, cmap=plt.cm.winter):
    """Make the line multi-coloured by plotting it in segments of length s which
        change in colour across the whole time series.
    """
    n = x.shape[0]
    for i in range(0, n-s, s):
        ax.plot(x[i:i+s+1], y[i:i+s+1], z[i:i+s+1],
                color=cmap(i/n), lw=.2, alpha=0.4)

def plot_phase_portrait_trajectory(X, Y, y0, time, A, name):
    """
    Plots a linear vector field in a streamplot, defined with X and Y coordinates
    and the matrix A and an example trajectory starting from y0.

    Parameters
    ----------
    X: numpy.array
            X coordinates of the vector field
    Y: numpy.array
            Y coordinates of the vector field
    y0: numpy.array
            Initial values for the system
    time: numpy.linspace
            Time steps for the dynamical system
    A:  numpy.matrix
            System matrix of the linear dynamical system
    name:  str
            Type of the phase portrait
    Returns
    -------
    ax0: matplotlib.axes.Axes
            Axes object of the plot
    """
    yt, time = solve_euler(lambda y: A@y, y0, time)
    ax0 = plot_phase_portrait(A, X, Y)

    # Change figure size for plotting in document
    ax0.figure.set_size_inches(*set_size(595, fraction=0.5))

    eigen_values = np.linalg.eigvals(A)

    # then plot the trajectory over it
    ax0.plot(yt[:, 0], yt[:, 1], c='r',
             label='Example trajectory'
             )
    ax0.scatter(yt[:, 0][0], yt[:, 1][0], c='r', marker="x")

    ax0.axis([-3, 3, -3, 3])
    ax0.set_title(fr'$\alpha$ = {a}, $λ_1$ = {np.round(eigen_values[0],2)}, $λ_2$ = {np.round(eigen_values[1],2)}')

    # prettify
    ax0.set_aspect("equal")

    ax0.figure.tight_layout()  # removes whitespace around the plot
    ax0.figure.savefig(f'figures/task1_{name}.png')

    return ax0
