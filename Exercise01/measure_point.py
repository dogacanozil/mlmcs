import numpy as np
import matplotlib.pyplot as plt

class MeasurePoint:
    """
    Defines a measuring point for creating the fundamental diagram of pedestrian flows.
    """

    def __init__(self, position):
        self.position = position
        self.avg_speed = []
        self.total_speed = 0
        self.density = []
        self.flow = []
        self.num_pedestrians = 0
        self.area = [(position[0] + i, position[1] + j) for i in range(2) for j in range(2)]

    def calculate_flow(self, pedestrian):
        """
        Calculates the flow values for a measure point by given Pedestrian which passes by the area of the MeasurePoint

        Args:
            pedestrian (str): Pedestrian object that passes by the measuring area
        """
        self.num_pedestrians += 1
        self.total_speed += pedestrian.desired_speed
        self.avg_speed.append(self.total_speed / self.num_pedestrians)
        self.density.append(self.num_pedestrians / 4)
        self.flow.append((self.total_speed / self.num_pedestrians) * (self.num_pedestrians / 4))

    def plot(self):
        """
        Plots the fundamental diagram of pedestrians flow for the MeasurePoint. It saves the created
        figures with the position of the MeasurePoint

        """
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10, 5))
        fig.suptitle(f'Fundamental Diagrams of Measurement Point({self.position[0]}, {self.position[1]})')

        ax1.scatter(self.density, self.avg_speed, alpha=0.5)
        ax1.set_title("Speed-density Diagram")
        ax1.set(xlabel='Density in P/m²', ylabel='Avg Speed in m/s')

        ax2.scatter(self.density, self.flow, alpha=0.5)
        ax2.set_title("Flow-density Diagram")
        ax2.set(xlabel='Density in P/m²', ylabel='Flow in P/ms')

        plt.savefig(f"figures/mp_{self.position[0]}_{self.position[1]}.png")
        plt.plot()

