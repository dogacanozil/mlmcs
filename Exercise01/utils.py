import numpy as np
from scenario_elements import Pedestrian, Scenario
from measure_point import MeasurePoint
import json


def generate_points(center, number):
    """
    Generates given number of points on a circle which center is the given center.
    Radius of the circle is determined by the half of the value of 0th index of the circle's center.

    Args:
        center (tuple): center of the circle for generating the points
        number (int): number of points to be created
    """
    coordinates = []

    r = center[0]/2
    theta = 2 * np.pi / number
    for i in range(0, number):
        x = np.round(center[0] + r * np.cos(theta*i))
        y = np.round(center[1] + r * np.sin(theta*i))
        coordinates.append([int(x), int(y)])
    return coordinates


def generate_pedestrians(scenario, number_pedestrians):
    """
    Generates given number of pedestrians for a given scenario.

    Args:
        scenario (scenario_elements.Scenario): Scenario object to filled with Pedestrian
        number_pedestrians (int): desired number of pedestrians
    Returns:
        scenario_elements.Scenario: Readed Scenario element with generated Pedestrians.
    """
    width, height = scenario.width, scenario.height
    center = (int(width/2), int(height/2))
    ped_coordinate = generate_points(center, number_pedestrians)
    scenario.recompute_target_distances()
    scenario.pedestrians = [Pedestrian((x, y), 2) for x, y in ped_coordinate]
    return scenario


def read_scenario(scenario_file):
    """
    Reads the attributes of te scneario and creates a Scenario object based on a scenario.json file.

    Returns:
        scenario_elements.Scenario: Created Scenario element with attributes based on .json file.
    """
    with open(scenario_file, "r") as file:
        data = json.loads(file.read())

    width, height = data["scenario_dimension"]
    sc = Scenario(width, height)
    sc.name = scenario_file.split("/")[1]

    for c_x, c_y in data["targets"]:
        sc.grid[c_x, c_y] = Scenario.NAME2ID['TARGET']

    for c_x, c_y in data["obstacles"]:
        sc.grid[c_x, c_y] = Scenario.NAME2ID['OBSTACLE']

    if "measurement_points" in data:
        for mes in data["measurement_points"]:
            temp = MeasurePoint(mes["position"])
            sc.measurement_points.append((temp))
            sc.measurement_area.extend(temp.area)
            for pos in temp.area:
                sc.grid[pos[0], pos[1]] = Scenario.NAME2ID['MEASUREMENT']
    sc.recompute_target_distances()

    sc.pedestrians = [Pedestrian(f["_position"], f["_desired_speed"]) for f in data["pedestrians"]]
    for p in sc.pedestrians:
        sc.grid[tuple(p._position)] = Scenario.NAME2ID['PEDESTRIAN']

    return sc


def save_scenario(width=50, height=50, peds=[], targets=[], obstacles=[], mes_points=[], json_file_name="scenario_default"):
    """
    Saves the given parameters of Scenario object into a json file.

    Args:
        width (int): width of the environment
        height (int): height of the environment
        peds (list): list of Pedestrian object to be saved
        targets (list): list of targets' coordinates to be saved
        obstacles (list): list of obstacles' coordinate to be saved
        mes_points (list): list of MeasurementPoint object to be saved
        json_file_name (str): name of the json file to be saved
    """
    dims = (width, height)
    peds = [ped.__dict__ for ped in peds]
    mes_points = [mes.__dict__ for mes in mes_points]

    with open(f"scenarios/{json_file_name}.json", "w") as file:
        a = {
            "scenario_dimension": dims,
            "pedestrians": peds,
            "targets": targets,
            "obstacles": obstacles,
            "measurement_points": mes_points
            }
        json.dump(a, file)
