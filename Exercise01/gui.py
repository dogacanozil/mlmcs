import sys
import time
import tkinter
from tkinter import Button, Canvas, Menu
from utils import *
from scenario_elements import Scenario, Pedestrian
import numpy as np
import copy
import os

global step
step = 0


class MainGUI():
    """
    Defines a simple graphical user interface.
    To start, use the `start_gui` method.
    """

    def restart_scenario(self, scenario, canvas, canvas_image):
        """
        Restart given scenario with initial values.

        Args:
            scenario (scenario_elements.Scenario): Existing scenario object to be updated
            canvas (tkinter.Canvas): Existing Canvas object to be updated
            canvas_image (int): Existing Canvas image object to be updated
        """
        global step
        saved_scenario = self.saved_scenario
        scenario.grid = copy.deepcopy(saved_scenario.grid)
        scenario.pedestrians = copy.deepcopy(saved_scenario.pedestrians)
        scenario.to_image(canvas, canvas_image)
        step = 0

    def step_scenario(self, scenario, canvas, canvas_image):
        """
        Moves the simulation forward by one step, and visualizes the result.

        Args:
            scenario (scenario_elements.Scenario): Existing scenario object to be updated
            canvas (tkinter.Canvas): Existing Canvas object to be updated
            canvas_image (int): Existing Canvas image object to be updated
        """
        global step
        scenario.update_step()
        scenario.to_image(canvas, canvas_image)
        step += 1
        print(f"Step {step}")

    def start_scenario(self, scenario, canvas, canvas_image, win):
        """
        Starts the simulation with all steps together.
        Visualizes a continuous movement for pedestrians in the scenario.

        Args:
            scenario (scenario_elements.Scenario): Existing scenario object to be updated
            canvas (tkinter.Canvas): Existing Canvas object to be updated
            canvas_image (int): Existing Canvas image object to be updated
            win (tkinter.Tk): Existing Tk object to be updated
        """
        start = time.time()
        self.stopped = False
        while scenario.pedestrians and not self.stopped:
            if scenario.name == "scenario_rimea2.json":
                if time.time() - start > 60:
                    break
            self.step_scenario(scenario, canvas, canvas_image)
            time.sleep(1)
            win.update()
        end = time.time()
        for mes in scenario.measurement_points:
            mes.plot()
            print(f"Position: {mes.position}\tDensity: {mes.density}\t Speed: {mes.avg_speed}")
        print(f"\nSimulation completed in {end - start:.3f} seconds")

    def load_scenario(self, scenario_name, scenario, canvas, canvas_image):
        """
        Loads the scenario named with scenario_name parameter, which is located under scenarios directory.
        Updates the attributes of existing scenario item with attributes taken by existing scenarios.
        Visualizes the created scenario case with canvas and canvas_image.
        Updates the saved_scenario in order to have proper restart.

        Args:
            scenario_name (str): Existing scenario object to be updated
            scenario (scenario_elements.Scenario): Existing scenario object to be updated
            canvas (tkinter.Canvas): Existing Canvas object to be updated
            canvas_image (int): Existing Canvas image object to be updated
            saved_scenario (scenario_elements.Scenario): Existing saved scenario object to be updated
        """

        print(f"You chose {scenario_name}")
        scenario_name = "scenarios/"+scenario_name
        sc = read_scenario(scenario_name)
        sc.name = scenario_name.split("/")[1]

        if (scenario_name == "scenarios/scenario_task3.json"):
            sc = generate_pedestrians(sc, 5)
        self.saved_scenario = copy.deepcopy(sc)
        scenario.name = sc.name
        scenario.width = sc.width
        scenario.height = sc.height
        scenario.grid = copy.deepcopy(sc.grid)
        scenario.pedestrians = copy.deepcopy(sc.pedestrians)
        scenario.pedestrians_arrived = []
        scenario.measurement_points = sc.measurement_points
        scenario.measurement_area = sc.measurement_area
        scenario.recompute_target_distances()
        self.saved_scenario.pedestrians = copy.deepcopy(scenario.pedestrians)
        scenario.to_image(canvas, canvas_image)

    def stop_scenario(self, ):
        """
        Stops the simulation
        """
        self.stopped = True

    def exit_gui(self, ):
        """
        Closes the GUI.
        """
        sys.exit()

    def start_gui(self, scenario_file=None):
        """
        Creates and shows a simple user interface with a menu and multiple buttons.
        Only one button works at the moment: "step simulation".
        Also creates a rudimentary, fixed Scenario instance with three Pedestrian instances and multiple targets.
        """
        win = tkinter.Tk()
        win.geometry('750x500')  # setting the size of the window
        win.title('Cellular Automata GUI')

        menu = Menu(win)
        win.config(menu=menu)
        file_menu = Menu(menu)
        menu.add_cascade(label='Simulation', menu=file_menu)
        file_menu.add_command(label='Restart', command=lambda: self.restart_scenario(sc, canvas, canvas_image))
        file_menu.add_command(label='Close', command=self.exit_gui)

        canvas = Canvas(win, width=Scenario.GRID_SIZE[0], height=Scenario.GRID_SIZE[1])  # creating the canvas
        canvas_image = canvas.create_image(5, 50, image=None, anchor=tkinter.NW)
        canvas.pack()

        if not scenario_file:
            sc = Scenario(100, 100)
            sc.grid[23, 25] = Scenario.NAME2ID['TARGET']
            sc.grid[23, 45] = Scenario.NAME2ID['TARGET']
            sc.grid[25, 45] = Scenario.NAME2ID['TARGET']

            sc.recompute_target_distances()

            sc.pedestrians = [
                Pedestrian((31, 2), 2.3),
                Pedestrian((1, 10), 2.1),
                Pedestrian((80, 70), 2.1)
            ]

        elif scenario_file == "scenarios/scenario_task3.json":
            sc = read_scenario(scenario_file)
            sc = generate_pedestrians(sc, 5)

        else:
            sc = read_scenario(scenario_file)

        self.saved_scenario = copy.deepcopy(sc)
        sc.to_image(canvas, canvas_image)

        scenarios = [
            ]
        path = 'scenarios'
        files = os.listdir(path)
        for f in files:
            scenarios.append(f)

        self.btn_start = Button(win, text='Start', command=lambda: self.start_scenario(sc, canvas, canvas_image, win))
        self.btn_start.place(x=20, y=10)

        self.btn_stop = Button(win, text='Stop', command=lambda: self.stop_scenario())
        self.btn_stop.place(x=120, y=10)

        self.btn_step = Button(win, text='Step', command=lambda: self.step_scenario(sc, canvas, canvas_image))
        self.btn_step.place(x=220, y=10)

        self.btn_restart = Button(win, text='Restart', command=lambda: self.restart_scenario(sc, canvas, canvas_image))
        self.btn_restart.place(x=320, y=10)

        variable = tkinter.StringVar()
        variable.set(scenarios[0])
        self.btn_load = tkinter.OptionMenu(win, variable, *scenarios, command=lambda x: self.load_scenario(x, sc, canvas, canvas_image))
        self.btn_load.place(x=420, y=10)

        win.mainloop()
