# MLMCS- Machine Learning in Crowd Modeling & Simulation / Exercise 1
## Introduction
It is a simple framework for the cellular automaton which is developed for Exercise 1 which is given in the course named Machine Learning in Crowd Modeling & Simulation at TUM.

## Getting Started
1. Install all required packages (python 3.10+). 
```
    > pip install -r requirements.txt  
```

2. Change your current directory to '/Exercise01'.
```
    > cd .\Exercise01
```
3. Start the GUI
```
    > python main.py
```

## Settings
### Scenarios
1. Add your own scenario as `.json` file under `scenarios` folder by following same format in default scenarios.

## Authors and acknowledgment
Felix Dietrich [Instructor]

Toni Latorre Canabate [Student/Developer]

Yusuf Kaan Akan [Student/Developer]

Doga Can Ozil [Student/Developer]
