import argparse
from gui import MainGUI


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str, nargs='?', default="scenarios/scenario.json")
    args = parser.parse_args()

    scenario_file = args.filename
    gui = MainGUI()
    gui.start_gui(scenario_file)
