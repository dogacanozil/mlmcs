import scipy.spatial.distance
from PIL import Image, ImageTk
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable


class Pedestrian:
    """
    Defines a single pedestrian.
    """

    def __init__(self, position, desired_speed):
        self.teleported = False
        self._position: tuple = position
        self._desired_speed = desired_speed

        # since the pedestrian cannot move diagonally but in a L-shaped
        # trajectory, we have to compensate by adding an "extra distance"
        # that the pedestrian should have to traverse in order to 
        # move at the desired speed
        self._remaining_traversal: float = 0
        self._trajectory: list[tuple[int, int]] = [(self._position[0], self._position[1])]

    @property
    def position(self):
        return self._position

    @property
    def desired_speed(self):
        return self._desired_speed

    @property
    def remaining_traversal(self):
        return self._remaining_traversal

    def get_neighbors(self, scenario):
        """
        Compute all neighbors in a 9 cell neighborhood of the current position.
        :param scenario: The scenario instance.
        :return: A list of neighbor cell indices (x,y) around the current position.
        """
        return [
            (int(x + self._position[0]), int(y + self._position[1]))
            for x in [-1, 0, 1]
            for y in [-1, 0, 1]
            if 0 <= x + self._position[0] < scenario.width and 0 <= y + self._position[1] < scenario.height and 0 < np.abs(x) + np.abs(y)
        ]

    def get_nearest_neighbors(self, scenario):
        """
        Compute all nearest neighbors in a 9 cell neighborhood of the current position.
        :param scenario: The scenario instance.
        :return: A list of neighbor cell indices (x,y) around the current position.
        """
        return [
            (int(x + self._position[0]), int(y + self._position[1]))
            for x in [-1, 0, 1]
            for y in [-1, 0, 1]
            if 0 <= x + self._position[0] < scenario.width and 0 <= y + self._position[1] < scenario.height and 0 < np.abs(x) + np.abs(y) < 2
        ]

    def pedestrian_interaction(
            self, position: tuple[int, int], scenario, rmax: float = 10.0
        ) -> float:
        """Computes the cost function corresponding to pedestrian interaction
        based on the distance to the closest target and the positions of the
        other pedestrians.

        Distance takes obstacles into account.

        Args:
            position (tuple[int, int]): coordinates of the given position
            scenario: current scenario
            rmax (float): pedestrian - pedestrian interaction term
        """
        cost = 0
        for pedestrian in list(filter(lambda p: p != self, scenario.pedestrians)):
            # if we don't want to take obstacles into account:
            # r = np.sqrt((position[0] - pedestrian._position[0])**2
            #            +(position[1] - pedestrian._position[1])**2)
            
            # if we want to take obstacles into account:
            r = np.abs(scenario.target_distance_grids[tuple(position)]
                      -scenario.target_distance_grids[tuple(pedestrian._position)])
            p_cost = np.exp(1 / (r**2 - rmax**2)) if r < rmax else 0
            cost = max(cost, p_cost)    
        return cost

    def update_step(self, scenario) -> bool:
        """
        Moves to the cell with the lowest cost to the target.
        If two cells have the same minimum cost, it moves to the closest cell
        (in Euclidean distance). 
        This takes obstacles and other pedestrians into account, i.e. two pedestrians
        cannot occupy the same place.

        :param scenario: The current scenario instance.

        Returns:
        
        bool: True if pedestrian has moved, False if not
        """
        neighbors = self.get_neighbors(scenario)
        #old_distance = scenario.target_distance_grids[self._position[0]][self._position[1]]
        #cost = old_distance + self.pedestrian_interaction(self._position, scenario)

        cost_function = lambda x: scenario.target_distance_grids[x[0], x[1]] + self.pedestrian_interaction(x, scenario)
        cost = cost_function(self._position)

        old_pos = self._position
        next_pos_candidates = [] 

        for (n_x, n_y) in neighbors:
            if (
                cost_function((n_x, n_y)) < cost 
                and scenario.grid[n_x, n_y] != scenario.NAME2ID['OBSTACLE']
                and scenario.grid[n_x, n_y] != scenario.NAME2ID['PEDESTRIAN']
            ):
                next_pos_candidates.append((n_x, n_y))

        # get minimum distance among all candidates
        if len(next_pos_candidates):
            minimum_cost = cost_function(min(next_pos_candidates, key=cost_function))

            candidates = list(filter(lambda x: minimum_cost == cost_function(x), next_pos_candidates))   

            if len(candidates):
                (n_x, n_y) = min(candidates, 
                    key=lambda x: + np.sqrt( (x[0] - self._position[0])**2 + (x[1] - self._position[1])**2 )
                )
                self._position = [n_x, n_y]

        if old_pos == self._position:
            return False
        else:
            if tuple(old_pos) in scenario.measurement_area:
                scenario.grid[tuple(old_pos)] = scenario.NAME2ID['MEASUREMENT']
            else:
                scenario.grid[tuple(old_pos)] = scenario.NAME2ID['EMPTY']
            if scenario.grid[tuple(self._position)] != Scenario.NAME2ID['TARGET']:
                scenario.grid[tuple(self._position)] = Scenario.NAME2ID['PEDESTRIAN']
            else:
                if scenario.name == "scenario_rimea2.json":
                    self.teleported = True
                    tmp = self._position
                    self._position = (0, tmp[1])
            return True


class Scenario:
    """
    A scenario for a cellular automaton.
    """
    GRID_SIZE = (500, 500)
    ID2NAME = {
        0: 'EMPTY',
        1: 'TARGET',
        2: 'OBSTACLE',
        3: 'PEDESTRIAN',
        4: 'MEASUREMENT'
    }
    NAME2COLOR = {
        'EMPTY': (255, 255, 255),
        'PEDESTRIAN': (255, 0, 0),
        'TARGET': (0, 0, 255),
        'OBSTACLE': (255, 0, 255),
        'MEASUREMENT': (0, 255, 0)
    }
    NAME2ID = {
        ID2NAME[0]: 0,
        ID2NAME[1]: 1,
        ID2NAME[2]: 2,
        ID2NAME[3]: 3,
        ID2NAME[4]: 4,
    }

    def __init__(self, width, height):
        if width < 1 or width > 1024:
            raise ValueError(f"Width {width} must be in [1, 1024].")
        if height < 1 or height > 1024:
            raise ValueError(f"Height {height} must be in [1, 1024].")

        self.width = width
        self.height = height
        self.grid_image = None
        self.grid = np.zeros((width, height))
        self.pedestrians = []
        self.pedestrians_arrived = []
        self.measurement_points = []
        self.measurement_area = []
        self.name = ""
        self.target_distance_grids = self.recompute_target_distances()

    def recompute_target_distances(self, obstacle_avoidance=True):
        """Computes distances from every point to its nearest target.

        Args:
            obstacle_avoidance (bool, optional): if True, distances are
            computed using the Dijkstra algorithm, thus taking obstacles
            into account. Otherwise, the Euclidean distance is computed.
        """
        if obstacle_avoidance:
            self.target_distance_grids = self.dijkstra()
        else:
            self.target_distance_grids = self.update_target_grid()

        return self.target_distance_grids

    def update_target_grid(self):
        """
        Computes the shortest distance from every grid point to the nearest target cell.
        This does not take obstacles into account.
        :returns: The distance for every grid cell, as a np.ndarray.
        """
        targets = []
        for x in range(self.width):
            for y in range(self.height):
                if self.grid[x, y] == Scenario.NAME2ID['TARGET']:
                    targets.append([y, x])  # y and x are flipped because they are in image space.
        if len(targets) == 0:
            return np.zeros((self.width, self.height))

        targets = np.row_stack(targets)
        x_space = np.arange(0, self.width)
        y_space = np.arange(0, self.height)
        xx, yy = np.meshgrid(x_space, y_space)
        positions = np.column_stack([xx.ravel(), yy.ravel()])

        # after the target positions and all grid cell positions are stored,
        # compute the pair-wise distances in one step with scipy.
        distances = scipy.spatial.distance.cdist(targets, positions)

        # now, compute the minimum over all distances to all targets.
        distances = np.min(distances, axis=0)

        return distances.reshape((self.width, self.height))

    def get_neighbors(self, position: list[int, int]) -> list[tuple[int, int]]:
        """Compute all neighbors in a 9 cell neighborhood of the current position.

        Args:
            position (list[int, int]): coordinates of current position

        Returns:
            list[list[int, int]]: coordinates of neighbors
        """
        return [
            (int(x + position[0]), int(y + position[1]))
            for x in [-1, 0, 1]
            for y in [-1, 0, 1]
            if 0 <= x + position[0] < self.width and 0 <= y + position[1] < self.height and 0 < np.abs(x) + np.abs(y)
        ]

    def get_nearest_neighbors(self, position: list[int, int]) -> list[tuple[int, int]]:
        """Compute all nearest neighbors in a 9 cell neighborhood of the current position.

        Args:
            position (list[int, int]): coordinates of current position

        Returns:
            list[list[int, int]]: coordinates of neighbors
        """
        return [
            (int(x + position[0]), int(y + position[1]))
            for x in [-1, 0, 1]
            for y in [-1, 0, 1]
            if 0 <= x + position[0] < self.width and 0 <= y + position[1] < self.height and 0 < np.abs(x) + np.abs(y) < 2
        ]

    def get_target_list(self) -> list[list[int, int]]:
        """Gets the list of targets in a grid
        """
        targets = []
        for x in range(self.width):
            for y in range(self.height):
                if self.grid[x, y] == Scenario.NAME2ID['TARGET']:
                    targets.append([y, x])  # y and x are flipped because they are in image space.
        
        return targets

    def get_obstacle_list(self) -> list[list[int, int]]:
        """Gets the list of obstacles in a grid
        """
        obstacles = []
        for x in range(self.width):
            for y in range(self.height):
                if self.grid[x, y] == Scenario.NAME2ID['OBSTACLE']:
                    obstacles.append([y, x])  # y and x are flipped because they are in image space.
        
        return obstacles

    def dijkstra(self) -> np.ndarray:
        """Computes the shortest distance from every grid point to the nearest target cell,
        using the Dijkstra algorithm.
        This takes obstacles into account.
        
        Returns:
            distances (np.ndarray): array of distances
        """

        distances = np.ones((self.height, self.width))*np.inf
        targets = list(map(tuple, self.get_target_list()))
        obstacles = list(map(tuple, self.get_obstacle_list()))

        for target in targets:
            visited = np.zeros(distances.shape, dtype=bool)  # True if point is visited
            # set all obstacles as visited (because we do not include them)
            for point, _ in np.ndenumerate(self.grid):
                if point in obstacles:
                    visited[point] = True

            # set distance = 0 to target positions
            distances[target] = 0
            visited[target] = True

            recent = [target]

            while np.count_nonzero(visited == False) > 0:
                recent_new = []

                for point in recent:
                    unvisited_neighbors = [
                        n for n in self.get_neighbors(list(point)) # 8 neighbors
                        if not visited[n] and n not in obstacles
                     ]

                    for neigh in unvisited_neighbors:
                        distances[neigh] = min(distances[neigh], distances[point] + 
                            np.sqrt((neigh[0] - point[0])**2 + (neigh[1] - point[1])**2)    
                        )

                # marks recently visited cells as visited
                for point in recent:
                    unvisited_neighbors = [
                        n for n in self.get_neighbors(list(point)) # 8 neighbors
                        if not visited[n] and n not in obstacles
                     ]
                    for neigh in unvisited_neighbors:
                        visited[neigh] = True
                        recent_new.append(neigh)
                recent = recent_new

        return np.transpose(distances)

    def update_step(self):
        """
        Updates the position of all pedestrians.
        This does not take obstacles or other pedestrians into account.
        Pedestrians can occupy the same cell.
        """

        for pedestrian in self.pedestrians:
            velocity = pedestrian.desired_speed
            pedestrian._remaining_traversal += velocity

            if tuple(pedestrian._position) in self.measurement_area:
                for mes in self.measurement_points:
                    if tuple(pedestrian._position) in mes.area:
                        mes.calculate_flow(pedestrian)

            while pedestrian.remaining_traversal > 1:
                # Keep updating if there is distance to traverse
                previous_position = pedestrian.position
                if pedestrian.update_step(self):
                    if not pedestrian.teleported:
                        # substract distance traversed in this update
                        pedestrian._remaining_traversal -= np.sqrt(
                            (previous_position[0] - pedestrian.position[0])**2
                            +(previous_position[1] - pedestrian.position[1])**2
                        )
                    else:
                        pedestrian.teleported = False
                else:  
                    # Assume waiting is equivalent to doing a step
                    pedestrian._remaining_traversal -= 1

                pedestrian._trajectory.append(pedestrian._position)  
                
                # if pedestrian arrives to target, remove it from list
                if self.grid[tuple(pedestrian._position)] == Scenario.NAME2ID['TARGET']:
                    self.pedestrians.remove(pedestrian)
                    self.pedestrians_arrived.append(pedestrian)
                    print(f"Pedestrian arrived")
                    break
                else:
                    self.grid[tuple(pedestrian._position)] = Scenario.NAME2ID['PEDESTRIAN']

    @staticmethod
    def cell_to_color(_id):
        return Scenario.NAME2COLOR[Scenario.ID2NAME[_id]]

    def target_grid_to_image(self, canvas, old_image_id):
        """
        Creates a colored image based on the distance to the target stored in
        self.target_distance_gids.
        :param canvas: the canvas that holds the image.
        :param old_image_id: the id of the old grid image.
        """
        im = Image.new(mode="RGB", size=(self.width, self.height))
        pix = im.load()
        for x in range(self.width):
            for y in range(self.height):
                target_distance = self.target_distance_grids[x][y]
                pix[x, y] = (max(0, min(255, int(10 * target_distance) - 0 * 255)),
                             max(0, min(255, int(10 * target_distance) - 1 * 255)),
                             max(0, min(255, int(10 * target_distance) - 2 * 255)))
        im = im.resize(Scenario.GRID_SIZE, Image.NONE)
        self.grid_image = ImageTk.PhotoImage(im)
        canvas.itemconfigure(old_image_id, image=self.grid_image)

    def to_image(self, canvas, old_image_id):
        """
        Creates a colored image based on the ids stored in self.grid.
        Pedestrians are drawn afterwards, separately.
        :param canvas: the canvas that holds the image.
        :param old_image_id: the id of the old grid image.
        """
        im = Image.new(mode="RGB", size=(self.width, self.height))
        pix = im.load()
        for x in range(self.width):
            for y in range(self.height):
                pix[x, y] = self.cell_to_color(self.grid[x, y])
        for pedestrian in self.pedestrians:
            x, y = pedestrian.position
            pix[x, y] = Scenario.NAME2COLOR['PEDESTRIAN']
        im = im.resize(Scenario.GRID_SIZE, Image.NONE)
        self.grid_image = ImageTk.PhotoImage(im)
        canvas.itemconfigure(old_image_id, image=self.grid_image)

    def plot_trajectory(self, filename=False):
        """Plots the grid with the trajectory of all files.

        Args:
            filename (str | bool, optional): If not False, path where
                the plot will be stored. Defaults to False.
        """
        im = Image.new(mode="RGB", size=(self.width, self.height))
        pix = im.load()
        for x in range(self.width):
            for y in range(self.height):
                pix[x, y] = self.cell_to_color(self.grid[x, y])
        for pedestrian in self.pedestrians:
            x, y = pedestrian.position
            pix[x, y] = Scenario.NAME2COLOR['PEDESTRIAN']

        fig, ax = plt.subplots(figsize=(5, 5))
        ax.imshow(im)
        for pedestrian in self.pedestrians:
            ax.scatter(*zip(pedestrian._trajectory[0]), c='r', s=10, facecolors='none', edgecolors='r')
            ax.plot(*zip(*pedestrian._trajectory), c='red', linewidth=0.5)
        for pedestrian in self.pedestrians_arrived:
            ax.scatter(*zip(pedestrian._trajectory[0]), c='r', s=10, facecolors='none', edgecolors='r')
            ax.plot(*zip(*pedestrian._trajectory), c='red', linewidth=0.5)
        plt.show()

        if filename:
            fig.savefig(filename)

    def plot_target_grid(self, filename=False):
        """Plots the target grid with distances to the nearest target.

        Args:
            filename (str | bool, optional): If not False, path where
                the plot will be stored. Defaults to False.
        """
        im = Image.new(mode="RGB", size=(self.width, self.height))
        pix = im.load()
        for x in range(self.width):
            for y in range(self.height):
                pix[x, y] = self.cell_to_color(self.grid[x, y])

        fig, ax = plt.subplots(figsize=(5, 5))
        scen = ax.imshow(im)
        dist = ax.imshow(np.transpose(self.target_distance_grids), cmap='gray')

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(dist, cax=cax, orientation='vertical')

        if filename:
            fig.savefig(filename)
