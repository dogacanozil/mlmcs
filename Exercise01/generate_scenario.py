"""
A script to generate test scenario and saves it as json file.
"""
from scenario_elements import Pedestrian
from measure_point import MeasurePoint
from utils import save_scenario
import numpy as np


if __name__ == '__main__':

    json_file_name = "scenario_bottleneck"

    width, height = (27, 12)


    peds = [Pedestrian((i, j), 2) for i in range(1, 6) for j in range(1, 11)]
    targets = [[26, 6]]
    obstacles = []

    obstacles.extend(list([0, j] for j in range(12)))
    obstacles.extend(list([i, j] for j in range(6) for i in [11, 15, 26]))
    obstacles.extend(list([i, j] for j in range(7, 12) for i in [11, 15, 26]))
    obstacles.extend(list([i, j] for i in range(11, 16) for j in range(6)))
    obstacles.extend(list([i, j] for i in range(11, 16) for j in range(7, 12)))
    obstacles.extend(list([i, j] for i in range(1, 11) for j in [0, 11]))
    obstacles.extend(list([i, j] for i in range(15, 27) for j in [0, 11]))       

    mes_points = []

    save_scenario(width, height, peds, targets, obstacles, mes_points, json_file_name)
