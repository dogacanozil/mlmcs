import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

def lorenz(t: float, state: tuple,
           sigma: float, beta: float, rho: float) -> list:
    """Lorenz system. Returns a list [dx/dt, dy/dt, dz/dt] to be
    used with PDE solvers."""
    x, y, z = state

    dx_dt = sigma * (y-x)
    dy_dt = x * (rho-z) - y
    dz_dt = x*y - beta*z

    return [sigma * (y - x),    # dx/dt
            x * (rho - z) - y,  # dy/dt
            x * y - beta * z    # dz/dt
            ]

def solve_lorenz_eqs(ts: np.ndarray, x0,
                     sigma: float, beta: float, rho: float) -> tuple:
    """Solves the Lorenz equations. Returns the positions at each time step
    in the form of a tuple (x, y, z)"""
    
    sol = solve_ivp(lorenz, (ts[0], ts[-1]), tuple(x0), t_eval=ts,
                     args=(sigma, beta, rho))  # object of type OdeSolution
    if sol:
        xyz = sol.y
        x, y, z = tuple(np.hsplit(xyz.T, 3))
        return x, y, z
    else:
        raise Exception('Lorenz equation could not be solved.')
