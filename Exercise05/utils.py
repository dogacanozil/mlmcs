import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.spatial.distance import cdist

def set_size(width, fraction=1, subplots=(1, 1)):
    """Set figure dimensions to avoid scaling in LaTeX.

    Parameters
    ----------
    width: float or string
            Document width in points, or string of predined document type
            For portrait A4, width=595
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy
    subplots: array-like, optional
            The number of rows and columns of subplots.
    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    if width == 'thesis':
        width_pt = 426.79135
    elif width == 'beamer':
        width_pt = 307.28987
    else:
        width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio * (subplots[0] / subplots[1])

    return (fig_width_in, fig_height_in)

# Radial basis function utilities
# -------------------------------
def radial_basis_function(center, data, eps, squared=True):
    if squared:
        eps = eps**2
    N = len(data)
    if type(data) == np.ndarray:
      N = data.shape[0]
    functions = np.zeros((N, center.shape[0]))
    for l in range(center.shape[0]):
        for j in range(N):
            functions[j, l] = np.exp((-1 / eps) * np.linalg.norm(center[l] - data[j])**2)
    return functions

def get_rbf_coeffs(x, f, centers, eps):
  """Gets the optimal coefficients for a radial basis function approximation."""
  phi_x = radial_basis_function(centers, x, eps)
  C = np.linalg.lstsq(phi_x, f, rcond=1e0-6)[0].T
  return C

def nonlinear_function(t, x, C, centers, eps, squared): 
    """dx/dt = f(t,x)"""
    # phi_x is the vector with all basis functions evaluated at x
    phi_x = radial_basis_function(centers, x, eps, squared)
    return (phi_x@C.T).ravel()

def radial_basis_function_faster(center, data, eps, squared=True):
    """A faster implementation of radial basis function"""
    ax = len(data.shape)-1
    if squared:
        eps = eps**2
    r = cdist(data, center)
    return np.exp(-1/eps * np.square(r))

def radial_basis_function_slow(center, data, eps, squared=True):
    ax = len(data.shape)-1
    if squared:
        eps = eps**2
    return np.exp((-1 / eps) * np.linalg.norm((center - data), axis=ax)**2)

def linear_system_task3(t, x, A):
    return A@x

def linear_system_task2(t, x, A):
    return A@x

def nonlinear_system_task3(t, x, C, centers, eps, squared, shape):
    """The non-linear system implementation for a faster calculation to use in scipy.integrate.solve_ivp"""
    y = np.zeros(shape)
    y[:,0] = x[::2]
    y[:,1] = x[1::2]

    phi_x = radial_basis_function_faster(centers, y, eps, squared)
    return (phi_x@C.T).ravel()

# PCA helper functions
# --------------------
def center_data(uncentered: np.ndarray):
    """Centers a dataset, i.e. each component is substracted the mean
       of the column.

    Args:
        uncentered (np.ndarray): data to be centered
    """
    centered = np.zeros(uncentered.shape)
    mean = uncentered.mean(axis=0) # computes mean of every column (component)
    for i in range(centered.shape[1]):
        centered[:, i] = uncentered[:, i] - mean[i]
    return centered, mean

def decompose_svd(x: np.ndarray):
    """Performs principal value decomposition of a matrix X.
       Returns matrices U, S, V such that X = USV^T

    Args:
        x (np.ndarray): matrix X
    """

    u, sigma, vt = np.linalg.svd(x)
    v = vt.T  # because np.linalg.svd returns V^T, not V
    s = np.zeros(x.shape)  # S might not be square!
    # construct S from sigma
    for i in range(len(sigma)):
        s[i, i] = sigma[i]

    return u, s, v

def reconstruct(u: np.ndarray, s: np.ndarray, v: np.ndarray, 
                r: int) -> np.ndarray:
    """Reconstructs (reduces) a dataset with a reduced number of 
       principal components. 

    Args:
        r (int): number of leading principal components to consider 
        s, u, v (np.ndarray): matrices from the singular value decomposition
            of the dataset
    """
    
    threshold = np.sort(np.diag(s))[-r] # r-th largest value
    s_reduced = np.copy(s)
    print()
    for i in range(len(np.diag(s))):
        if s_reduced[i, i] < threshold:
            s_reduced[i, i] = 0
    data_reduced = u @ s_reduced @ v.T
    return data_reduced

# Time delay - for Takens' theorem
def time_delay(data: np.ndarray, dt: float, n_delays: int):
    """Returns a tuple with multiple time delays of a time series data set. 
    The data corresponding to no delay is also included.

    Args:
        data (np.ndarray): time series
        dt (float): delay magnitude, in time step units (e.g. rows)
        n_delays (int): number of delays to be returned
    """

    delays = []
    for i in range(n_delays):
        delay = data[
            (i*dt):(-dt*(n_delays-i))
            ]
        delays.append(delay)
    delays.append(
        data[n_delays*dt:]
    )
    return tuple(delays)


def plot_phase_portrait(A, X, Y):
    """
    Plots a linear vector field in a streamplot, defined with X and Y coordinates and the matrix A.
    """
    UV = A@np.row_stack([X.ravel(), Y.ravel()])
    U = UV[0,:].reshape(X.shape)
    V = UV[1,:].reshape(X.shape)

    fig = plt.figure(figsize=(15, 15))

    # gs = gridspec.GridSpec(nrows=3, ncols=2, height_ratios=[1, 1, 2])

    #  Varying density along a streamline
    # ax0 = fig.add_subplot(gs[0, 0])
    ax0 = fig.add_subplot()
    ax0.streamplot(X, Y, U, V, density=[0.5, 1])
    ax0.set_title('Streamplot for linear vector field A*$x_0$');
    ax0.set_aspect(1)
    return ax0
