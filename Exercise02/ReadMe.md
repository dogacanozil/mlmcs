# MLMCS- Machine Learning in Crowd Modeling & Simulation / Exercise 2
## Introduction
Using and analyzing simulation software which is named Vadere.

## Getting Started

1. Change your current directory to '/Exercise02'.
```
    > cd .\Exercise01
```
2. Start the GUI
```
    > python main.py
```

## Authors and acknowledgment
Felix Dietrich [Instructor]

Toni Latorre Canabate [Student/Developer]

Yusuf Kaan Akan [Student/Developer]

Doga Can Ozil [Student/Developer]
