import os
import json


if __name__ == '__main__':
    # Opening JSON file
    new_filename = "RIMEA6_SFM_added_pedestrian"
    with open('scenarios/RIMEA6_SFM.scenario') as json_file:
        data = json.load(json_file)
        data['name'] = new_filename
        data['scenario']['topography']['dynamicElements'].append(
            {
                'attributes': {
                    'id': 4, 'shape': {'x': 0.0, 'y': 0.0, 'width': 1.0, 'height': 1.0, 'type': 'RECTANGLE'},
                    'visible': True, 'radius': 0.2, 'densityDependentSpeed': False, 'speedDistributionMean': 1.34,
                    'speedDistributionStandardDeviation': 0.26, 'minimumSpeed': 0.5, 'maximumSpeed': 2.2,
                    'acceleration': 2.0, 'footstepHistorySize': 4, 'searchRadius': 1.0,
                    'walkingDirectionSameIfAngleLessOrEqual': 45.0, 'walkingDirectionCalculation': 'BY_TARGET_CENTER'
                    }, 'source': None, 'targetIds': [3], 'nextTargetListIndex': 0, 'isCurrentTargetAnAgent': False,
                'position': {'x': 11.0, 'y': 1.0}, 'velocity': {'x': 0.0, 'y': 0.0},
                'freeFlowSpeed': 1.2374163348807505, 'followers': [], 'idAsTarget': -1, 'isChild': False,
                'isLikelyInjured': False, 'psychologyStatus': {
                'mostImportantStimulus': None, 'threatMemory': {'allThreats': [], 'latestThreatUnhandled': False},
                'selfCategory': 'TARGET_ORIENTED', 'groupMembership': 'OUT_GROUP',
                'knowledgeBase': {'knowledge': [], 'informationState': 'NO_INFORMATION'}, 'perceivedStimuli': [],
                'nextPerceivedStimuli': []
                }, 'healthStatus': None, 'infectionStatus': None, 'groupIds': [], 'groupSizes': [], 'agentsInGroup': [],
                'trajectory': {'footSteps': []}, 'modelPedestrianMap': None, 'type': 'PEDESTRIAN'
                })

        with open(f"scenarios/{new_filename}.scenario", "w") as file:
            json.dump(data, file, indent=2)

    PATH_TO_SCENARIOS = f"scenarios/{new_filename}.scenario"
    PATH_TO_OUTPUTS = f"output/"
    os.system(f'java -jar vadere-console.jar scenario-run --scenario-file "{PATH_TO_SCENARIOS}" --output-dir="{PATH_TO_OUTPUTS}"')
