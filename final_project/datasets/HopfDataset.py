import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader, random_split, SubsetRandomSampler
import numpy as np
import pandas as pd
from scipy.integrate import solve_ivp
from utils import *

import torchvision.transforms as transforms
transform = transforms.Compose([transforms.ToTensor()])

class HopfDataset(Dataset):
    """HopfDataset Dataset for ODE"""

    def __init__(self, parameters):
        """_summary_

        Args:
            parameters (_type_): _description_
        """
    
        self.method = parameters["method"]
        self.name = parameters["name"]
        self.device = parameters["device"]

        data = pd.read_parquet(f"data/{self.name}_{self.method}.parquet")

        self.inputs = torch.tensor(data[["x0", "y0", "par0"]].values, dtype=torch.float32, device=self.device)
        
        self.labels = torch.tensor(data[["x1", "y1"]].values, dtype=torch.float32, device=self.device)
        
    def __len__(self):
        return len(self.inputs)

    def __getitem__(self, index):
        return self.inputs[index], self.labels[index]