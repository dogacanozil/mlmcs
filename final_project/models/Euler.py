import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader, random_split, SubsetRandomSampler

from pytorch_lightning import Trainer
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from models.BaseModel import BaseModel

class Euler(pl.LightningModule):
    """Neural network for the explicit Euler method"""
    def __init__(self, time_step, d_input, d_output, d_hidden, n_hidden, datasets, batch_size):
        """Initializes the neural network

        Args:
            time_step (float): time step of the simulated solver
            d_input (int): number of input cells
            d_output (int): number of output cells
            d_hidden (int): number of cells per hidden layer
            n_hidden (int): number of hidden layers
            datasets (tuple[TensorDataset, 3]): training, validation and test sets
            batch_size (int): batch size
        """
        super().__init__()
        self.train_data, self.val_data, self.test_data = datasets

        self.batch_size = batch_size
        self.time_step = time_step
        self.d_output = d_output
        self.mlp = BaseModel(d_input, d_output, d_hidden, n_hidden)

    def forward(self, x):
        return x[:, :self.d_output] + self.time_step * self.mlp(x)
    
    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.mlp(x) * self.time_step + x[:, :self.d_output]
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("train_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(self.parameters(), lr=0.01)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, 'min')
        return {
            'optimizer': optimizer,
            'scheduler': scheduler,
            'monitor': 'metric_to_track'}
 
    def validation_step(self, batch, batch_idx, print_str="val"):
        x, y = batch
        y_hat = self.mlp(x) * self.time_step + x[:, :self.d_output]
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("val_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss
    
    def test_step(self, batch, batch_idx, print_str="test"):
        x, y = batch
        y_hat = self.mlp(x) * self.time_step + x[:, :self.d_output]
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("test_loss", loss, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss

    def train_dataloader(self):
        return DataLoader(self.train_data, batch_size=self.batch_size, shuffle=True, num_workers=2)

    def val_dataloader(self):
        return DataLoader(self.val_data, batch_size=self.batch_size, shuffle=False, num_workers=2)

    def test_dataloader(self):
        return DataLoader(self.test_data, batch_size=self.batch_size, shuffle=False, num_workers=2)