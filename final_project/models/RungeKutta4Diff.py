import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader, random_split, SubsetRandomSampler

from pytorch_lightning import Trainer
import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from models.BaseModel import BaseModel

class RK4(pl.LightningModule):
    """Neural network for the explicit Runge-Kutta method of order 4"""
    def __init__(self, time_step, d_input, d_output, d_hidden, n_hidden, datasets, batch_size):
        """Initializes the neural network

        Args:
            time_step (float): time step of the simulated solver
            d_input (int): number of input cells
            d_output (int): number of output cells
            d_hidden (int): number of cells per hidden layer
            n_hidden (int): number of hidden layers
            datasets (tuple[TensorDataset, 3]): training, validation and test sets
            batch_size (int): batch size
        """
        super().__init__()
        self.train_data, self.val_data, self.test_data = datasets

        self.batch_size = batch_size
        self.time_step = time_step
        self.d_output = d_output
        self.mlp1 = BaseModel(d_input, d_output, d_hidden, n_hidden)
        self.mlp2 = BaseModel(d_input, d_output, d_hidden, n_hidden)
        self.mlp3 = BaseModel(d_input, d_output, d_hidden, n_hidden)


    def forward(self, x):
        xn = x[:, :self.d_output]
        params = x[:, self.d_output:]

        # kN_ -> only the coordinates
        # kN  -> coordinates + parameters

        k1_ = self.mlp1(x)
        k1 = torch.hstack((k1_, params))

        k2_ = self.mlp2(
          torch.hstack((xn + self.time_step * k1_/2, params))
        )
        k2 = torch.hstack((k2_, params))

        k3_ = self.mlp2(
          torch.hstack((xn + self.time_step * k2_/2, params))
        )
        k3 = torch.hstack((k3_, params))
        
        k4_ = self.mlp3(
          torch.hstack((xn + self.time_step * k3_, params))
        )
        k4 = torch.hstack((k3_, params))

        return xn + (k1_ + 2*k2_ + 2*k3_ + k4_)*self.time_step/6
    
    def training_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self.forward(x)
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("loss", {"train_loss": loss}, on_step=True, on_epoch=True, prog_bar=True, logger=True)
        return loss

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters(), lr=0.001)
 
    def validation_step(self, batch, batch_idx, print_str="val"):
        x, y = batch
        y_hat = self.forward(x)
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("loss", {"val_loss": loss}, on_step=True, on_epoch=True, prog_bar=False, logger=True)
        self.log("val_loss", loss, on_step=True, on_epoch=True, prog_bar=False, logger=True)
        return loss
    
    def test_step(self, batch, batch_idx, print_str="test"):
        x, y = batch
        y_hat = self.forward(x)
        loss = torch.nn.functional.mse_loss(y_hat, y)
        self.log("test_loss", loss, on_step=True, on_epoch=True, prog_bar=False, logger=True)
        return loss

    def train_dataloader(self):
        return DataLoader(self.train_data, batch_size=self.batch_size, shuffle=True, num_workers=2)

    def val_dataloader(self):
        return DataLoader(self.val_data, batch_size=self.batch_size, shuffle=False, num_workers=2)

    def test_dataloader(self):
        return DataLoader(self.test_data, batch_size=self.batch_size, shuffle=False, num_workers=2)