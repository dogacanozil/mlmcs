import torch
import torch.nn as nn

class BaseModel(nn.Module):
    def __init__(self, d_input, d_output, d_hidden, n_hidden):
        super(BaseModel, self).__init__()

        self.input = nn.Linear(d_input, d_hidden)

        self.hidden = nn.Sequential(
            *[nn.Sequential(nn.Linear(d_hidden, d_hidden), nn.ReLU()) for _ in range(n_hidden)])
        self.output = nn.Linear(d_hidden, d_output)

        self.network = nn.Sequential(
          self.input,
          nn.ReLU(),
          self.hidden,
          self.output
        )
        
        for layer in self.hidden:
            nn.init.xavier_uniform_(layer[0].weight)
        nn.init.xavier_uniform_(self.input.weight)    
        nn.init.xavier_uniform_(self.input.weight)

    def forward(self, x):
        return self.network(x)
