import numpy as np

def andronov_hopf_bifurcation(t, x, alpha=0):
    """
    Calculates the Andronov-Hopf bifurcation from given x and returns the velocities x1_dot and x2_dot.
    Implemented specifically to be used faster with scipy.integrate.solve_ivp.
    Parameters
    ----------
    t: numpy.array
            Time values of the system
    x: numpy.array
            x1 and x2 stacked horizontally
    alpha: numpy.linspace
            Parameter of the system
    """
    x1 = x[::2]
    x2 = x[1::2]

    x1_dot = alpha*x1 - x2 - x1*(x1**2 + x2**2)
    x2_dot = x1 + alpha*x2 - x2*(x1**2 + x2**2)
    return np.insert(x2_dot, np.arange(len(x1_dot)), x1_dot)

def lorenz(t: float, state: tuple,
           sigma: float, beta: float, rho: float) -> list:
    """Lorenz system. Returns a list [dx/dt, dy/dt, dz/dt] to be
    used with PDE solvers.
    sigma, beta, rho are the parameters of the equations.
    """
    x = state[::3]
    y = state[1::3]
    z = state[2::3]

    x_dot = sigma * (y - x)    # dx/dt
    y_dot = x * (rho - z) - y  # dy/dt
    z_dot = x * y - beta * z    # dz/dt

    return np.stack([x_dot,y_dot,z_dot]).T.flatten()

def linear_system(t, x, alpha=0):
    """
    2D linear system of ODE's
    ----------
    t: numpy.array
            Time values of the system
    x: Tuple of numpy.array
            x1 and x2 values
    alpha: numpy.linspace
            Parameter of the system (NOT USED)
    """
    x1, x2 = x
    x1_dot = 3/2 - x1 + (1/2)*x2
    x2_dot = 0 + (1/2)*x1 - x2
    return [x1_dot, x2_dot]