import os
import matplotlib.pyplot as plt
import numpy as np

from scipy.integrate import solve_ivp

import torch
import pytorch_lightning as pl

def predict(model: pl.LightningModule,
    n_dimensions: int, n_params: int, n_timesteps: int,
    initial_condition: np.ndarray, params):
    """Predicts the evolution of the system.

    Args:
        model (pl.LightningModule): model simulating the desired numerical method
        n_dimensions (int): number of dimensions of the system
        n_params (int): number of parameters of the system
        n_timesteps (int): number of timesteps to be predicted
        initial_condition (np.ndarray): initial condition -- shape (1, n_dimensions)
        params (np.ndarray | None): parameters -- shape (1, n_params)
    """
    
    if initial_condition.shape != (1, n_dimensions):
        raise Exception(f'initial_condition array must be horizontal and of shape (1, {n_dimensions}).')
    if params is not None and params.shape != (1, n_params):
        raise Exception(f'params array must be horizontal and of shape (1, {n_dimensions})')

    sol = torch.Tensor(initial_condition)  # Results are stored here
    
    # Prepare data to feed into the neural network
    if n_params:
        y_0 = torch.hstack((
            sol.reshape((1, n_dimensions)), torch.Tensor(params)
        ))
    else:
        y_0 = sol.reshape((1, n_dimensions))

    for _ in range(n_timesteps):
        # Predict next time step
        y_1 = model(y_0.reshape((1, n_dimensions + n_params)))
        sol = torch.vstack((sol, y_1))  # add new time step to solution array
        y_0 = torch.hstack((y_1, torch.Tensor(params))) if n_params else y_1  # input for next time step
        
    return sol

def test(f, model: pl.LightningModule, 
    n_dimensions: int, n_params: int, n_timesteps: int,
    initial_condition: np.ndarray, params):
    """Tests the neural network with a dynamical system.

    Args:
        f (callable): evolution operator of the system
        model (pl.LightningModule): neural network
        n_dimensions (int): number of dimensions of the system
        n_params (int): number of parameters of the system
        n_timesteps (int): number of timesteps to be simulated
        initial_condition (np.ndarray): initial condition -- shape is (1, n_dimensions)
        params (np.ndarray | None): parameters -- if specified, shape is (1, n_params)

    Returns:
        single_test_nn (np.ndarray): results from the neural network
        single_test_odesolver (np.ndarray): results from a higher-order ODE solver

        For both arrays, each row is the state of the system at one time step, like
        [x_1(t), x_2(t), ... x_n(t)].
    """

    # Solve with our neural network
    single_test_nn = predict(model, n_dimensions, n_params, n_timesteps,
                             initial_condition.reshape((1, n_dimensions)),
                             params.reshape((1, n_params)))

    # Solve with a higher-order ODE solver
    single_test_odesolver = np.copy(initial_condition)
    dt = model.time_step
    sol = solve_ivp(f, [0, dt*n_timesteps],
                    y0=initial_condition,
                    args=(params),
                    dense_output=True, t_eval=dt*np.arange(n_timesteps+1))
    single_test_odesolver = sol.y.T

    return single_test_nn.detach().numpy(), single_test_odesolver

def plot_test_results(single_test_nn, single_test_odesolver, dt: float,
                      output_folder_path: str, params) -> None:

    n_timesteps = single_test_odesolver.shape[0]

    # COMPARE x(t) 
    plt.figure()
    plt.plot(dt * np.arange(n_timesteps), single_test_odesolver[:, 0], 'o-', label=r'$x(t_n)$, actual')
    plt.plot(dt * np.arange(n_timesteps), single_test_nn[:, 0], 'o-', label=r'$x_n$, predicted')

    plt.title(rf'$y_0=({single_test_nn[0, 0]:.2f}, {single_test_nn[0, 1]:.2f})$, params={params}')
    plt.xlabel('t')
    plt.ylabel(r'$x$')
    plt.legend()
    plt.savefig(os.path.join(output_folder_path, 'x.png'))

    # COMPARE y(t)
    plt.figure()
    plt.plot(dt * np.arange(n_timesteps), single_test_odesolver[:, 1], 'o-', label=r'$y(t_n)$, actual')
    plt.plot(dt * np.arange(n_timesteps), single_test_nn[:, 1], 'o-', label=r'$y_n$, predicted')

    plt.title(rf'$y_0=({single_test_nn[0, 0]:.2f}, {single_test_nn[0, 1]:.2f})$, params={params}')
    plt.xlabel('t')
    plt.ylabel(r'$y(t)$')
    plt.legend()
    plt.savefig(os.path.join(output_folder_path, 'y.png'))

    # COMPARE TRAJECTORIES
    plt.figure()
    plt.plot(single_test_odesolver[:, 0], single_test_odesolver[:, 1], 'o', label=r'Actual')
    plt.plot(single_test_nn[:, 0], single_test_nn[:, 1], '+', label=r'Predicted')
    plt.plot(single_test_nn[0, 0], single_test_nn[0, 1], 'rx', label='Initial condition')
    plt.title(rf'$y_0=({single_test_nn[0, 0]:.2f}, {single_test_nn[0, 1]:.2f})$, params={params}')

    plt.xlabel('x')
    plt.ylabel(r'y')
    plt.legend()
    plt.savefig(os.path.join(output_folder_path, 'traj.png'))
