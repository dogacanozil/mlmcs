import numpy as np
from scipy.integrate import solve_ivp

def andronov_hopf_bifurcation(t, x, alpha=0):
    """
    Calculates the Andronov-Hopf bifurcation from given x and returns the velocities x1_dot and x2_dot.
    Parameters
    ----------
    t: numpy.array
            Time values of the system
    x: Tuple of numpy.array
            x1 and x2 values
    alpha: numpy.linspace
            Parameter of the system
    """
    x1, x2 = x
    x1_dot = alpha*x1 - x2 - x1*(x1**2 + x2**2)
    x2_dot = x1 + alpha*x2 - x2*(x1**2 + x2**2)
    return [x1_dot, x2_dot]

def lorenz(t: float, state: tuple,
           sigma: float, beta: float, rho: float) -> list:
    """Lorenz system. Returns a list [dx/dt, dy/dt, dz/dt] to be
    used with PDE solvers.
    sigma, beta, rho are the parameters of the equations.
    """
    x, y, z = state

    return [sigma * (y - x),    # dx/dt
            x * (rho - z) - y,  # dy/dt
            x * y - beta * z    # dz/dt
           ]

def linear_system(t, x, alpha=0):
    """
    2D linear system of ODE's
    ----------
    t: numpy.array
            Time values of the system
    x: Tuple of numpy.array
            x1 and x2 values
    alpha: numpy.linspace
            Parameter of the system (NOT USED)
    """
    x1, x2 = x
    x1_dot = 3/2 - x1 + (1/2)*x2
    x2_dot = 0 + (1/2)*x1 - x2
    return [x1_dot, x2_dot]

# %%
def generate_learning_set(function, n_dimensions, n_params, n_points: int,
                          dt: float, n_gen_timesteps: int, n_gen_params: int
                          ) -> np.ndarray:
    """Generates the learning set for a dynamical system.

    Args:
        function (callable): dynamical system

    Raises:
        Exception: if the dynamical system's function is not implemented

    Returns:
        np.ndarray: learning set [*x*, params]
    """
    y_0 = np.array([], dtype=np.float64).reshape(0, n_dimensions)  # first time step
    y_1 = np.array([], dtype=np.float64).reshape(0, n_dimensions)  # next time step
    y_params = np.array([], dtype=np.float64).reshape(0, n_params) # parameters

    # Random initial conditions 
    # -------------------------
    # here I generate them with a normal distribution because we already know
    # that the most important behavior is around the origin (where there is a fixed point if I remember correctly)

    if function == lorenz:
        random_1 = np.random.normal(loc=10, scale=1.0, size=n_points)
        random_2 = np.random.normal(loc=10, scale=1.0, size=n_points)
        random_3 = np.random.normal(loc=10, scale=1.0, size=n_points)
        random_initial_points = (random_1, random_2, random_3)
    elif function == andronov_hopf_bifurcation:
        random_1 = np.random.normal(loc=0.0, scale=4.0, size=n_points)
        random_2 = np.random.normal(loc=0.0, scale=4.0, size=n_points)
        random_initial_points = (random_1, random_2)
    else:
        raise Exception('This function is not implemented.')
    assert len(random_initial_points) == n_dimensions

    # Random parameters
    # -- for Hopf
    if function == andronov_hopf_bifurcation:
        random_alpha = np.random.normal(loc=0.0, scale=3, size=n_gen_params)
        random_parameters = (random_alpha,)
    # -- for Lorenz
    elif function == lorenz:
        random_sigma = np.random.normal(loc=10, scale=1, size=n_gen_params)
        random_beta = np.random.normal(loc=8/3, scale=0.5, size=n_gen_params)
        random_rho = np.random.normal(loc=14, scale=2, size=n_gen_params)
        random_parameters = (random_sigma, random_beta, random_rho)

    # ------------------------------------------------------------------------------
    # Generate test data from random initial conditions and make it evolve
    # one time step using our (known) solution
    for point in zip(*random_initial_points):
        for params_set in zip(*random_parameters):
            initial = np.array([*point]).reshape((1, n_dimensions))
            sol = solve_ivp(function, [0, n_gen_timesteps * dt], initial.reshape(n_dimensions,),
                            args=(params_set),
                            dense_output=True, t_eval=dt*np.arange(n_gen_timesteps))
            evolution = sol.y.T
            y_0 = np.vstack((y_0, evolution[:-1, :]))
            y_1 = np.vstack((y_1, evolution[1:, :]))
            if n_params:
              y_params = np.vstack((y_params, np.tile(params_set, (n_gen_timesteps-1, 1))))
              y = np.hstack((y_0, y_1, y_params))
            else:
              y = np.hstack((y_0, y_1))

    assert y.shape == (n_points * n_gen_params * (n_gen_timesteps-1), 2*n_dimensions + n_params)
    return y
