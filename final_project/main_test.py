# -*- coding: utf-8 -*-
"""
MAIN TEST FOR THE NEURAL NETWORKS

1. Define the dynamical system and its properties
2. Generate trajectories from random initial conditions and parameters
3. If the model is not stored yet, train the model with these trajectories
4. Load the trained weights of the model
5. For different initial conditions 
"""

# from google.colab import drive
# drive.mount('/content/drive')

# Commented out IPython magic to ensure Python compatibility.
# %cd /content/drive/MyDrive/final_project

# Commented out IPython magic to ensure Python compatibility.
# %load_ext autoreload
# %autoreload 2

# To install PyTorch Lightning:
# !pip install pytorch-lightning

import random

import matplotlib.pyplot as plt
import numpy as np

from scipy.integrate import solve_ivp
from sklearn.model_selection import train_test_split

import torch
import pytorch_lightning as pl

from models import BaseModel, Euler, RungeKutta4Same, RungeKutta4Diff
try:
    from models import RK4
except ImportError:
    pass

import os

from utils.common import * 
from utils.systems import *  # dynamical systems are stored here
from utils.test_utils import *

# Deterministic initialization of each random processes
# It is a MUST for replicating the same results

seed = 42 
random.seed(seed)
np.random.seed(seed)
torch.manual_seed(seed)
# torch.backends.cudnn.benchmark = False
# torch.backends.cudnn.deterministic = True

"""## Function definition"""
# -----------------------------------------------------------------------------
# DYNAMICAL SYSTEM PARAMETERS
f = lorenz  # evolution function
dt = 0.01 # time step

n_params = 3  # number of parameters
n_dimensions = 3  # number of dimensions

# NEURAL NETWORK PARAMETERS
solver = 'rk4'

rk4_solver = None
if solver == 'rk4':
    rk4_solver = RungeKutta4Same

n_training_epochs = 300  # number of epochs to train the model
n_hidden_layers = 6  # number of hidden layers
n_cells_per_layer = 10  # number of cells per hidden layer
batch_size = 1000  # batch size
plot_training_data = False  # whether or not to plot the training points / trajectories

# TEST PARAMETERS
single_test_timesteps = 2500  # timesteps to be simulated in the test
n_gen_testpoints = 5  # number of random test points to be generated
plot_results = True # whether or not to plot the trajectories 

test_initial_points = (
        np.random.normal(loc=10, scale=2.0, size=n_gen_testpoints),
        np.random.normal(loc=10, scale=2.0, size=n_gen_testpoints),
        np.random.normal(loc=10, scale=2.0, size=n_gen_testpoints),
    )
test_parameters = [[10, 8./3., 28], [10, 8./3., 0.5]]
# -----------------------------------------------------------------------------

# OUTPUT PARAMETERS
results_folder = os.path.join(os.getcwd(), 'results', 'RK4_DiffMLP_Lorenz')  # Folder where all results are stored
torch_model_file = os.path.join('trained_models',
                                f'RK4_DiffMLP_Lorenz_L_{n_hidden_layers}_H_{n_cells_per_layer}.model')

if not os.path.isdir(results_folder):
    os.makedirs(results_folder)

"""## Generation of points"""

"""Our learning set consists of an array with $N$ rows and $2d + n_{\text{params}}$ columns, where $d$ is the dimensionality of the system and $n_{\text{params}}$ is the number of parameters. Each row contains $[\vec{y}_n, \vec{y}_{n+1}, \vec{\theta}]^\top$."""

y = generate_learning_set(f, n_dimensions, n_params, n_points=30, dt=dt, 
                          n_gen_timesteps=1000, n_gen_params=8)
y_0 = y[:, :n_dimensions]
y_1 = y[:, n_dimensions:2*n_dimensions]
if n_params:
    y_params = y[:, -n_params:]

print(f'Learning set consists of {y.shape[0]} points.')

# We plot the training data 

y1 = y_0[:, 0]
y2 = y_0[:, 1]

if plot_training_data:
    plt.title('Training trajectories')
    # plt.xlim([-4, 4])
    # plt.ylim([-4, 4])
    plt.scatter(y1, y2, s=1, alpha=0.2 
                # c=y[:, -1]  # Comment this if n_params != 1
                )

# Define training, validation and test sets

if n_params:
  learn_y0, test_y0, learn_y1, test_y1 = train_test_split(
      np.hstack((y_0, y_params)), 
      y_1,
      test_size=0.2)
else:
  learn_y0, test_y0, learn_y1, test_y1 = train_test_split(y_0, y_1,
      test_size=0.2)

train_y0, val_y0, train_y1, val_y1 = train_test_split(
    learn_y0, learn_y1, test_size=0.2)

train_dataset = torch.utils.data.TensorDataset(
    torch.Tensor(train_y0),
    torch.Tensor(train_y1)
)
    
val_dataset = torch.utils.data.TensorDataset(
    torch.Tensor(val_y0),
    torch.Tensor(val_y1)
)
    
test_dataset = torch.utils.data.TensorDataset(
    torch.Tensor(test_y0),
    torch.Tensor(test_y1)
)

# Define number of input units 
if len(y_0.shape) == 1:
    ndim_system = 1
else:
    ndim_system = y_0.shape[1]

"""## Definition of the neural network

Found in `BaseModel.py` and `Euler.py`.
"""

if solver == 'rk4':
    model = rk4_solver.RK4(time_step=dt, d_input=n_dimensions+n_params,
                d_output=n_dimensions, d_hidden=n_cells_per_layer,
                n_hidden=n_hidden_layers, 
                datasets=(train_dataset, val_dataset, test_dataset),
                batch_size=batch_size
    )
elif solver == 'euler':
    model = Euler.Euler(time_step=dt, d_input=n_dimensions+n_params,
                d_output=n_dimensions, d_hidden=n_cells_per_layer,
                n_hidden=n_hidden_layers, 
                datasets=(train_dataset, val_dataset, test_dataset),
                batch_size=batch_size
    )   
else:
    raise Exception(f'Model {solver} does not exist. Choose: [euler, rk4]')

"""## Training the neural network"""

if not os.path.isfile(torch_model_file):
    trainer = pl.Trainer(max_epochs=n_training_epochs)
    trainer.fit(model=model)
    torch.save(model, torch_model_file)

"""## Predicting the evolution of the system"""

model = torch.load(torch_model_file)
# model.eval()

k = 0
with open(os.path.join(results_folder, f'Test_{k}.txt'), 'w') as results_file:
    results_file.write('#k;initial_condition;params\n')
    for point in zip(*test_initial_points):
        for param in test_parameters:
            initial = np.array([*point])
            params = np.array([*param])

            np.savetxt(results_file, np.hstack((
                np.array([k]).reshape((1, 1)),
                initial.reshape((1, n_dimensions)),
                params.reshape((1, n_params))
                )))

            single_test_nn, single_test_odesolver = test(
                f, model, n_dimensions, n_params,
                n_timesteps=single_test_timesteps, 
                initial_condition=initial,
                params=params
            )
            if not os.path.isdir(os.path.join(results_folder, f'Test_{k}')):
                os.makedirs(os.path.join(results_folder, f'Test_{k}'))
            np.savetxt(os.path.join(results_folder, f'Test_{k}', 'single_test_nn.txt'), single_test_nn)
            np.savetxt(os.path.join(results_folder, f'Test_{k}', 'single_test_odesolver.txt'), single_test_odesolver)
            if plot_results:
                plot_test_results(single_test_nn, single_test_odesolver, dt,
                                    os.path.join(results_folder, f'Test_{k}'), params)

            k += 1