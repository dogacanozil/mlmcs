# MLMCS- Machine Learning in Crowd Modeling & Simulation Exercises
## Description
This repo is a collective folder for exercises which are given in Machine Learning in Crowd Modeling & Simulation course, offered at TUM in WS 22/23 semester. In each exercise folder you can find the relevant codes for the given exercise.

## Authors and acknowledgment
Felix Dietrich [Instructor]

Toni Latorre Canabate [Student/Developer]

Yusuf Kaan Akan [Student/Developer]

Doga Can Ozil [Student/Developer]

## License
TUM - Machine Learning in Crowd Modeling & Simulation Course [IN2106]
